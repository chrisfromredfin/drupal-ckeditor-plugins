#!/usr/bin/env php
<?php

/**
 * @file
 * Generates the satis.json file.
 */

function generate_satis_json($dest, $core_versions, $core_plugins, $extra_plugins) {
  $satis = [
    'name' => 'panopoly/drupal-ckeditor-plugins',
    'description' => 'Drupal CKEDitor Plugins',
    'homepage' => 'https://gitlab.com/panopoly/drupal-ckeditor-plugins',
    'repositories' => [],
    'require-all' => TRUE,
  ];

  foreach ($core_versions as $version => $core_requirement) {
    foreach ($core_plugins as $plugin) {
      $satis['repositories'][] = [
        "type" => "package",
        "package" => [
          "name" => "drupal-ckeditor-plugin/{$plugin}",
          "version" => $version,
          "type" => "drupal-library",
          "dist" => [
            "url" => "https://download.ckeditor.com/{$plugin}/releases/{$plugin}_{$version}.zip",
            "type" => "zip"
          ],
          "require" => [
            "drupal/core" => $core_requirement,
          ],
        ]
      ];
    }
  }

  foreach ($extra_plugins as $plugin => $versions) {
    foreach ($versions as $version => $url) {
      $satis['repositories'][] = [
        "type" => "package",
        "package" => [
          "name" => "drupal-ckeditor-plugin/{$plugin}",
          "version" => $version,
          "type" => "drupal-library",
          "dist" => [
            "url" => $url,
            "type" => "zip"
          ]
        ]
      ];
    }
  }

  file_put_contents($dest, json_encode($satis, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));
}

function read_text_file($file) {
  $lines = explode("\n", file_get_contents($file));
  return array_filter($lines, function ($line) {
    return !empty($line) && $line[0] !== '#';
  });
}

function read_json_file($file) {
  return json_decode(file_get_contents($file), TRUE);
}

function main() {
  $root = dirname(__FILE__);

  $core_versions = [];
  foreach (read_text_file("$root/core-versions.txt") as $core_version_line) {
    $core_version_parts = array_map('trim', explode(':', $core_version_line));
    $core_versions[$core_version_parts[0]] = $core_version_parts[1];
  }

  $core_plugins = read_text_file("$root/core-plugins.txt");

  $extra_plugins = read_json_file("$root/extra-plugins.json");

  generate_satis_json("$root/satis.json", $core_versions, $core_plugins, $extra_plugins);
}

if (php_sapi_name() === 'cli') {
  main();
}

